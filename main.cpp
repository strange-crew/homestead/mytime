#include <board.h>
#include <stdio.h>
#include <xtimer.h>
#include <lvgl/lvgl.h>

#include <display.h>
#include <tick_circle.h>
#include <analog_clock.h>
#include <periph/flashpage.h>
#include <mtd.h>
#include <mtd_mapper.h>
#include <errno.h>

#define FIRST_AVAILABLE_PAGE_ADDR 94208

mtd_mapper_parent_t mtd_parent = MTD_PARENT_INIT(MTD_0);

mtd_mapper_region_t region1 = {
    .mtd = {
        .driver = &mtd_mapper_driver,
        .sector_count = 128,
        .pages_per_sector = PINETIME_NOR_PAGES_PER_SECTOR,
        .page_size = PINETIME_NOR_PAGE_SIZE
    },
    .parent = &mtd_parent,
    .sector = 0
};

mtd_mapper_region_t region2 = {
    .mtd = {
        .driver = &mtd_mapper_driver,
        .sector_count = 128,
        .pages_per_sector = PINETIME_NOR_PAGES_PER_SECTOR,
        .page_size = PINETIME_NOR_PAGE_SIZE
    },
    .parent = &mtd_parent,
    .sector = 128
};



int main()
{
    board_init();
    initialize_display();

    auto clock = create_analog_clock(lv_scr_act());
    lv_obj_set_pos(clock, 0, 0);
    lv_obj_set_size(clock, 240, 240);

    mtd_dev_t * dev = &region1.mtd;

    mtd_init(dev);
    auto result = mtd_erase_sector(dev, 0, 1);
    printf("result: %d\n", result);

    if (result == -ENODEV) puts("nodev");
    if (result == -ENOTSUP) puts("notsup");
    if (result == -EOVERFLOW) puts("overflow");
    if (result == -EIO) puts("io");

    char derp = 0;
    result = mtd_read(dev, &derp, 0, 1);
    printf("result: %d\n", result);
    printf("first read: %d\n", (int)derp);

    derp = 2;
    result = mtd_write(dev, &derp, 0, 1);
    printf("result: %d\n", result);
    derp = 0;
    result = mtd_read(dev, &derp, 0, 1);
    printf("result: %d\n", result);
    printf("post write read: %d\n", (int)derp);

    derp = 1;
    result = mtd_write(dev, &derp, 0, 1);
    printf("result: %d\n", result);
    derp = 0;
    result = mtd_read(dev, &derp, 0, 1);
    printf("result: %d\n", result);
    printf("post write read: %d\n", (int)derp);

#if 0
    auto page = flashpage_page((void*)FIRST_AVAILABLE_PAGE_ADDR);
    auto sz = flashpage_size(page);
    printf("size: %d\n", sz);
    flashpage_erase(page);

    char * buffer = new char[4096];
    flashpage_read(page, buffer);
    printf("value: %lu\n", *(uint32_t*)buffer);

    *(int*)buffer = 32;
    flashpage_write((void*)FIRST_AVAILABLE_PAGE_ADDR, buffer, 4);
    flashpage_read(page, buffer);
    printf("value: %lu\n", *(uint32_t*)buffer);

    *(int*)buffer = 19;
    flashpage_write((void*)FIRST_AVAILABLE_PAGE_ADDR, buffer, 4);
    flashpage_read(page, buffer);
    printf("value: %lu\n", *(uint32_t*)buffer);

    flashpage_erase(page);

    *(int*)buffer = 19;
    flashpage_write((void*)FIRST_AVAILABLE_PAGE_ADDR, buffer, 4);
    flashpage_read(page, buffer);
    printf("value: %lu\n", *(uint32_t*)buffer);
#endif

    while (true)
    {
        lv_task_handler();
        xtimer_msleep(10);
        clock_tick(clock);
    }
    puts("Hello world");
}
