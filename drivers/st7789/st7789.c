#include <st7789.h>
#include <st7789_internal.h>
#include <stdio.h>
#include <xtimer.h>
#include <debug.h>

static void acquire_spi_(st7789_t const* dev)
{
    spi_acquire(
            dev->params->spi,
            dev->params->cs_pin,
            dev->params->spi_mode,
            dev->params->spi_clock
        );
}
static void release_spi_(st7789_t const* dev)
{
    spi_release(dev->params->spi);
}

static void start_write_cmd_(st7789_t const* dev, uint8_t cmd, bool cont)
{
    gpio_clear(dev->params->dcx_pin);
    spi_transfer_byte(dev->params->spi, dev->params->cs_pin, cont, cmd);
    gpio_set(dev->params->dcx_pin);
}

static void write_cmd_(st7789_t const* dev, uint8_t cmd, uint8_t const* data, size_t len)
{
    start_write_cmd_(dev, cmd, len > 0 ? true:false);
    if (len > 0)
    {
        spi_transfer_bytes(
                dev->params->spi,
                dev->params->cs_pin,
                false,
                data,
                NULL,
                len
            );
    }
}

static void write_data_(st7789_t const* dev, uint8_t const* data, size_t len)
{
    spi_transfer_bytes(
            dev->params->spi,
            dev->params->cs_pin,
            false,
            data,
            NULL,
            len
        );
}

static void hardware_reset_(st7789_t * dev)
{
    if (gpio_is_valid(dev->params->rst_pin))
    {
        gpio_init(dev->params->rst_pin, GPIO_OUT);
        gpio_clear(dev->params->rst_pin);
        xtimer_msleep(120);
        gpio_set(dev->params->rst_pin);
    }
    xtimer_msleep(120);
}


void set_area_(st7789_t * dev, uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2)
{
    uint8_t command_params[8] = {
        x1 >> 8,
        x1 & 0xFF,
        x2 >> 8,
        x2 & 0xFF,
        y1 >> 8,
        y1 & 0xFF,
        y2 >> 8,
        y2 & 0xFF
    };
    write_cmd_(dev, ST7789_CMD_CASET, command_params, 4);
    write_cmd_(dev, ST7789_CMD_RASET, command_params + 4, 4);
}




int st7789_init(st7789_t * dev, st7789_params_t const* params)
{
    uint8_t command_params[4] = {0};
    dev->params = params;

    gpio_init(dev->params->dcx_pin, GPIO_OUT);

    int result = spi_init_cs(dev->params->spi, dev->params->cs_pin);
    if (result != SPI_OK)
    {
        DEBUG("[st7789] init: error initializing cs pin [%i]\n", result);
        return -1;
    }
    
    hardware_reset_(dev);

    acquire_spi_(dev);
    {
        write_cmd_(dev, ST7789_CMD_SWRESET, NULL, 0);
        xtimer_msleep(120);

        write_cmd_(dev, ST7789_CMD_SLPOUT, NULL, 0);

        // colmod
        command_params[0] = 0x55; // todo: why?
        write_cmd_(dev, ST7789_CMD_COLMOD, command_params, 1);
        xtimer_msleep(10);

        // mdactl
        command_params[0] = 0x00;
        write_cmd_(dev, ST7789_CMD_MADCTL, command_params, 1);
        
        set_area_(dev, 0, dev->params->width, 0, dev->params->height);

        // inversion on 
        if (dev->params->inverted)
            write_cmd_(dev, ST7789_CMD_INVON, NULL, 0);

        // normal mode
        write_cmd_(dev, ST7789_CMD_NORON, NULL, 0);

        // disp on
        write_cmd_(dev, ST7789_CMD_DISPON, NULL, 0);
    }
    release_spi_(dev);

    return 0;
}


void st7789_fill(st7789_t * dev, uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2, uint16_t color)
{
    int32_t numpix = (x2 - x1 + 1) * (y2 - y1 + 1);

    // DEBUG

    acquire_spi_(dev);
    {
        set_area_(dev, x1, x2, y1, y2);
        start_write_cmd_(dev, ST7789_CMD_RAMWR, true);

        uint8_t encoded[2] = {
            color >> 8,
            color & 0xFF
        };
        for (int32_t i = 0 ; i < (numpix - 1); ++i)
            write_data_(dev, encoded, 2);
    }
    release_spi_(dev);
}


void st7789_pixmap(st7789_t * dev, uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2, uint16_t * colors)
{
    size_t numpix = (x2 - x1 + 1) * (y2 - y1 + 1);

    acquire_spi_(dev);
    {
        set_area_(dev, x1, x2, y1, y2);
        start_write_cmd_(dev, ST7789_CMD_RAMWR, true);
        write_data_(dev, (uint8_t*)colors, numpix * 2);
    }
    release_spi_(dev);
}
