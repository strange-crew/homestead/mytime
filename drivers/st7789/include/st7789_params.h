#ifndef ST7789_PARAMS_H
#define ST7789_PARAMS_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef ST7789_PARAM_SPI
#  define ST7789_PARAM_SPI SPI_DEV(0)
#endif

#ifndef ST7789_PARAM_SPI_CLK
#  define ST7789_PARAM_SPI_CLK SPI_CLK_10MHZ
#endif

#ifndef ST7789_PARAM_SPI_MODE
#  define ST7789_PARAM_SPI_MODE SPI_MODE_3
#endif

#ifndef ST7789_PARAM_CS_PIN
#  define ST7789_PARAM_CS_PIN GPIO_PIN(0, 25)
#endif

#ifndef ST7789_PARAM_DCX_PIN
#  define ST7789_PARAM_DCX_PIN GPIO_PIN(0, 18)
#endif

#ifndef ST7789_PARAM_RST_PIN
#  define ST7789_PARAM_RST_PIN GPIO_PIN(0, 26)
#endif

#ifndef ST7789_PARAM_RGM
#  define ST7789_PARAM_RGB true
#endif

#ifndef ST7789_PARAM_INVERT
#  define ST7789_PARAM_INVERT true
#endif

#ifndef ST7789_PARAM_HEIGHT
#  define ST7789_PARAM_HEIGHT 240
#endif

#ifndef ST7789_PARAM_WIDTH
#  define ST7789_PARAM_WIDTH 240
#endif


#define ST7789_PARAMS { \
    .spi = ST7789_PARAM_SPI,  \
    .spi_clock = ST7789_PARAM_SPI_CLK, \
    .spi_mode = ST7789_PARAM_SPI_MODE, \
    .cs_pin = ST7789_PARAM_CS_PIN, \
    .dcx_pin = ST7789_PARAM_DCX_PIN, \
    .rst_pin = ST7789_PARAM_RST_PIN, \
    .rgb = ST7789_PARAM_RGB, \
    .inverted = ST7789_PARAM_INVERT, \
    .height = ST7789_PARAM_HEIGHT, \
    .width = ST7789_PARAM_WIDTH \
}


static st7789_params_t st7789_params[] = {
    ST7789_PARAMS,
};


#ifdef __cplusplus
}
#endif




#endif
