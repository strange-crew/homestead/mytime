#ifndef ST7789_INTERNAL_H
#define ST7789_INTERNAL_H

#ifdef __cplusplus
extern "C" {
#endif

#define ST7789_CMD_NOOP 0x00  // No operation

#define ST7789_CMD_SWRESET      0x01    // software reset
#define ST7789_CMD_RDDID        0x04    // read display id
#define ST7789_CMD_RDDST        0x09    // read display status
#define ST7789_CMD_RDDPM        0x0A    // read display power mode
#define ST7789_CMD_RDDMADCTL    0x0B    // read display MADCTL
#define ST7789_CMD_RDDCOLMOD    0x0C    // read display pixel format
#define ST7789_CMD_RDDIM        0x0D    // read display image mode
#define ST7789_CMD_RDDSM        0x0E    // read display signal mode
#define ST7789_CMD_RDDSDR       0x0F    // read display self diagnostic result
#define ST7789_CMD_SLPIN        0x10    // sleep in
#define ST7789_CMD_SLPOUT       0x11    // sleep out
#define ST7789_CMD_PTLON        0x12    // partial display mode on
#define ST7789_CMD_NORON        0x13    // normal display mode on
#define ST7789_CMD_INVOFF       0x20    // display inversion off
#define ST7789_CMD_INVON        0x21    // display inversion on
#define ST7789_CMD_GAMSET       0x26    // gamma set
#define ST7789_CMD_DISPOFF      0x28    // display off
#define ST7789_CMD_DISPON       0x29    // display on
#define ST7789_CMD_CASET        0x2A    // column address set
#define ST7789_CMD_RASET        0x2B    // row address set
#define ST7789_CMD_RAMWR        0x2C    // memory write
#define ST7789_CMD_RAMRD        0x2E    // memory read
#define ST7789_CMD_PTLAR        0x30    // partial area
#define ST7789_CMD_VSCRDEF      0x33    // vertical scroll definition
#define ST7789_CMD_TEOFF        0x34    // tearing effect line off
#define ST7789_CMD_TEON         0x35    // tearing effect line on
#define ST7789_CMD_MADCTL       0x36    // memory access control
#define ST7789_CMD_VSCSAD       0x37    // vertical scroll start address of RAM
#define ST7789_CMD_IDMOFF       0x38    // idle mode off
#define ST7789_CMD_IDMON        0x39    // idle mode on
#define ST7789_CMD_COLMOD       0x3A    // interface pixel format
#define ST7789_CMD_WRMEMC       0x3C    // write memory continue
#define ST7789_CMD_RDMEMC       0x3E    // read memory continue
#define ST7789_CMD_STE          0x44    // set tear scanline
#define ST7789_CMD_GSCAN        0x45    // get scanline
#define ST7789_CMD_WRDISBV      0x51    // write display brightness
#define ST7789_CMD_RDDISBV      0x52    // read display brightness value
#define ST7789_CMD_WRCTRLD      0x53    // write CTRL display
#define ST7789_CMD_RDCTRLD      0x54    // read CTRL value display
#define ST7789_CMD_WRCACE       0x55    // write content adaptive brightness control and color enhancement
#define ST7789_CMD_RDCABC       0x56    // read content adaptive brightness control
#define ST7789_CMD_WRCABCMB     0x5E    // write CABC minimum brightness
#define ST7789_CMD_RDCABCMD     0x5F    // read CABC minimum brightness
#define ST7789_CMD_RDABCSDR     0x68    // read automatic brightness control self diagnostic result
#define ST7789_CMD_RDID1        0xDA    // read id 1
#define ST7789_CMD_RDID2        0xDB    // read id 2
#define ST7789_CMD_RDID3        0xDC    // read id 3
#define ST7789_CMD_RAMCTRL      0xB0    // RAM control
#define ST7789_CMD_RGBCTRL      0xB1    // RGB interface control
#define ST7789_CMD_PORCTRL      0xB2    // porch setting
#define ST7789_CMD_FRCTRL1      0xB3    // frame rate control 1
#define ST7789_CMD_PARCTRL      0xB5    // partial control
#define ST7789_CMD_GCTRL        0xB7    // gate control
#define ST7789_CMD_GTADJ        0xB8    // gate on timing adjustment
#define ST7789_CMD_DGMEN        0xBA    // digital gamma enable
#define ST7789_CMD_VCOMS        0xBB    // VCOM setting
#define ST7789_CMD_POWSAVE      0xBC    // power saving mode
#define ST7789_CMD_DLPOFFSAVE   0xBD    // display off power save
#define ST7789_CMD_LCMCTRL      0xC0    // LCM control
#define ST7789_CMD_IDSET        0xC1    // ID code setting
#define ST7789_CMD_VDVVRHEN     0xC2    // VDV and VRH command enable
#define ST7789_CMD_VRHS         0xC3    // VRH set
#define ST7789_CMD_VDVS         0xC4    // VDV set
#define ST7789_CMD_VCMOFSET     0xC5    // VCOM offset set
#define ST7789_CMD_FRCTRL2      0xC6    // frame rate control in normal mode
#define ST7789_CMD_CABCCTRL     0xC7    // CABC control
#define ST7789_CMD_REGSEL1      0xC8    // register value selection 1
#define ST7789_CMD_REGSEL2      0xCA    // register value selection 2
#define ST7789_CMD_PWMFRSEL     0xCC    // PWM frequency selection
#define ST7789_CMD_PWCTRL1      0xD0    // power control 1
#define ST7789_CMD_VAPVENEN     0xD2    // enable VAP/VEN signal output
#define ST7789_CMD_CMD2EN       0xDF    // command 2 enable
#define ST7789_CMD_PVGAMCTRL    0xE0    // positive voltage gamma control
#define ST7789_CMD_NVGAMCTRL    0xE1    // negative voltage gamma control
#define ST7789_CMD_DGMLUTR      0xE2    // digital gamma lookup table for red
#define ST7789_CMD_DGMLUTB      0xE3    // digital gamma lookup table for blue
#define ST7789_CMD_GATECTRL     0xE4    // gate control
#define ST7789_CMD_SPI2EN       0xE7    // SPI2 enable
#define ST7789_CMD_PWCTRL2      0xE8    // power control 2
#define ST7789_CMD_EQCTRL       0xE9    // equalize time control
#define ST7789_CMD_PROMCTRL     0xEC    // program mode control
#define ST7789_CMD_PROMEN       0xFA    // program mode enable
#define ST7789_CMD_NVMSET       0xFC    // NVM setting
#define ST7789_CMD_PROMACT      0xFE    // program action


#ifdef __cplusplus
}
#endif



#endif
