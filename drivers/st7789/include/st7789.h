#ifndef ST7789_H
#define ST7789_h

#include <periph/spi.h>
#include <periph/gpio.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {

    spi_t spi;
    spi_clk_t spi_clock;
    spi_mode_t spi_mode;

    gpio_t cs_pin;
    gpio_t dcx_pin;
    gpio_t rst_pin;

    bool rgb;
    bool inverted;

    uint16_t height;
    uint16_t width;

} st7789_params_t;

typedef struct {
    st7789_params_t const* params;
} st7789_t;

int st7789_init(st7789_t * dev, st7789_params_t const* params);

void st7789_fill(st7789_t * dev, uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2, uint16_t color);

void st7789_pixmap(st7789_t * dev, uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2, uint16_t * color);

#ifdef __cplusplus
}
#endif


#endif
