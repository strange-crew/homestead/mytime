APPLICATION = mytime
BOARD ?= pinetime
RIOTBASE ?= $(CURDIR)/../RIOT

PROGRAMMER ?= openocd
OPENOCD_DEBUG_ADAPTER ?= raspi
SWCLK_PIN ?= 25
SWDIO_PIN ?= 24
RIOT_TERMINAL ?= semihosting

COMPILE_COMMANDS_PATH ?= $(CURDIR)/compile_commands.json

USEMODULE += stdio_semihosting
#USEMODULE += stdin
USEMODULE += st7789

USEMODULE += display
USEMODULE += analog_clock
USEMODULE += mtd_mapper

FEATURES_REQUIRED += cpp
FEATURES_REQUIRED += periph_flashpage
FEATURES_REQUIRED += periph_flashpage_pagewise

EXTERNAL_MODULE_DIRS += $(CURDIR)/drivers
EXTERNAL_MODULE_DIRS += $(CURDIR)/modules

BOOT_BOOTSLOT_SIZE = 0x8060
BOOT_IMAGE_HEADER_SIZE = 32

IMGTOOL ?= $(CURDIR)/../myboot/modules/mcuboot/mcuboot-tree/scripts/imgtool.py
#orreride IMGTOOL := $(abspath $(IMGTOOL))
ROM_OFFSET = 0x8080
IMAGE_OFFSET = 0x8060

SIGNED_BINFILE = $(BINDIR)/$(APPLICATION)-boot.bin

$(SIGNED_BINFILE): $(BINFILE)
	$(IMGTOOL) sign -S 0x77020 -v "1.0.0" --align 4 --pad-header --header-size $(BOOT_IMAGE_HEADER_SIZE) $(BINFILE) $(SIGNED_BINFILE)

imagesign: $(SIGNED_BINFILE)


flash-boot: FLASHFILE=$(SIGNED_BINFILE)
flash-boot: export IMAGE_OFFSET=$(BOOT_BOOTSLOT_SIZE)
flash-boot: $(SIGNED_BINFILE) $(FLASHDEPS)
	$(flash-recipe)

include $(RIOTBASE)/Makefile.include

