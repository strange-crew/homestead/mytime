#include <analog_clock.h>
#include <lvgl/lvgl.h>
#include <circle_calc.h>
#include <stdio.h>
#include <xtimer.h>

struct clock_ext_t
{
    lv_point_t second_pts[2];
    lv_obj_t * second_hand;

    lv_point_t minute_pts[2];
    lv_obj_t * minute_hand;

    lv_point_t hour_pts[2];
    lv_obj_t * hour_hand;
};

static lv_design_cb_t ancestor_design;

static lv_design_res_t analog_clock_design_(lv_obj_t * btn, const lv_area_t * clip_area, lv_design_mode_t mode);

static lv_coord_t coord_(int i) { return (lv_coord_t)i; }

lv_obj_t * create_analog_clock(lv_obj_t * parent)
{
    auto obj = lv_obj_create(parent, nullptr);
    auto ext = (clock_ext_t*)lv_obj_allocate_ext_attr(obj, sizeof(clock_ext_t));

    ext->second_hand = lv_line_create(obj, nullptr);
    lv_line_set_points(ext->second_hand, ext->second_pts, 2);

    ext->minute_hand = lv_line_create(obj, nullptr);
    lv_line_set_points(ext->minute_hand, ext->minute_pts, 2);

    ext->hour_hand = lv_line_create(obj, nullptr);
    lv_line_set_points(ext->hour_hand, ext->hour_pts, 2);

    static lv_style_t background_style;
    lv_style_init(&background_style);
    lv_style_set_bg_color(&background_style, LV_STATE_DEFAULT, LV_COLOR_BLACK);
    lv_style_set_border_color(&background_style, LV_STATE_DEFAULT, LV_COLOR_BLACK);
    lv_obj_add_style(obj, LV_OBJ_PART_MAIN, &background_style);

    static lv_style_t second_line_style;
    lv_style_init(&second_line_style);
    lv_style_set_line_width(&second_line_style, LV_STATE_DEFAULT, 3);
    lv_style_set_line_color(&second_line_style, LV_STATE_DEFAULT, LV_COLOR_RED);
    lv_obj_add_style(ext->second_hand, LV_LINE_PART_MAIN, &second_line_style);

    static lv_style_t hands_line_style;
    lv_style_init(&hands_line_style);
    lv_style_set_line_width(&hands_line_style, LV_STATE_DEFAULT, 3);
    lv_style_set_line_color(&hands_line_style, LV_STATE_DEFAULT, LV_COLOR_WHITE);

    lv_obj_add_style(ext->minute_hand, LV_LINE_PART_MAIN, &hands_line_style);
    lv_obj_add_style(ext->hour_hand, LV_LINE_PART_MAIN, &hands_line_style);

    if (ancestor_design == nullptr) ancestor_design = lv_obj_get_design_cb(obj);
    lv_obj_set_design_cb(obj, analog_clock_design_);

    return obj;
}

static lv_point_t translate_point_(lv_point_t mid, lv_point_t pt)
{
    return {
        coord_(mid.x + pt.x),
        coord_(mid.y + pt.y)
    };
}

void clock_tick(lv_obj_t * clock)
{
    auto width = lv_obj_get_width(clock);
    auto radius = width / 2;
    auto mid = lv_point_t{ coord_(radius), coord_(radius) };
    //mid.x += clock->coords.x1;
    //mid.y += clock->coords.y1;

    auto ext = (clock_ext_t*)lv_obj_get_ext_attr(clock);
    auto outer = radius * .8;
    auto inner = radius * .1;

    auto time = xtimer_now_usec64() + 21600000000;
    auto sec = time % 60000000;
    auto secf = sec * .000001;
    auto ang = secf * 6;

    ext->second_pts[0] = translate_point_(mid, calc_circle_point(outer, ang));
    ext->second_pts[1] = translate_point_(mid, calc_circle_point(inner, ang));

    lv_line_set_points(ext->second_hand, ext->second_pts, 2);

    auto min = time % 3600000000;
    auto minf = min * .000001 / 60;
    ang = minf * 6;

    ext->minute_pts[0] = translate_point_(mid, calc_circle_point(outer, ang));
    ext->minute_pts[1] = translate_point_(mid, calc_circle_point(inner, ang));

    lv_line_set_points(ext->minute_hand, ext->minute_pts, 2);

    auto hour = time % 43200000000;
    auto hourf = hour * .000001 / 3600;
    ang = hourf * 30;

    outer = radius * .5;

    ext->hour_pts[0] = translate_point_(mid, calc_circle_point(outer, ang));
    ext->hour_pts[1] = translate_point_(mid, calc_circle_point(inner, ang));

    lv_line_set_points(ext->hour_hand, ext->hour_pts, 2);
    static auto last = 0;

    if (((int)secf) > last)
    {
        printf("%d:%d:%d\n", (int)hourf, (int) minf, (int)secf);
        last = (int)secf;
        if (last == 59) last = -1;
    }
}


static lv_design_res_t analog_clock_design_(lv_obj_t * clock_obj, lv_area_t const* clip_area, lv_design_mode_t mode)
{
    ancestor_design(clock_obj, clip_area, mode);

    if (mode == LV_DESIGN_DRAW_MAIN)
    {
        lv_draw_line_dsc_t line_desc;
        lv_draw_line_dsc_init(&line_desc);

        line_desc.color = lv_color_make(0xFF, 0xFF, 0xFF);
        line_desc.width = 3;

        auto width = lv_obj_get_width(clock_obj);
        auto radius = width / 2;
        auto in_long = radius * .9;
        auto in_short = radius * .95;
        auto mid = lv_point_t{ coord_(radius), coord_(radius) };
        mid.x += clock_obj->coords.x1;
        mid.y += clock_obj->coords.y1;

        for (auto i = 0; i < 60; ++i)
        {
            auto inner = i % 5 == 0 ? in_long:in_short;
            lv_point_t points[] = {
                translate_point_(mid, calc_circle_point(radius, i * 6)),
                translate_point_(mid, calc_circle_point(inner, i * 6))
            };
            lv_draw_line(&points[0], &points[1], clip_area, &line_desc);
        }
    }

    return LV_RES_OK;
}
