import math

for i in range(90):
    x = math.sin(math.radians(i))
    if i % 10 == 0: 
        print("")
        print("    ",end="")
    print(f"{int(x * 10000):4}, ",end="")


