#ifndef ANALOG_CLICK_H
#define ANALOG_CLICK_H

#include <lvgl/lvgl.h>

lv_obj_t * create_analog_clock(lv_obj_t * parent);

void clock_tick(lv_obj_t * clock_obj);

#endif
