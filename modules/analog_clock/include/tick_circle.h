#ifndef TICK_CIRCLE_H
#define TICK_CIRCLE_H
#include <lvgl/lvgl.h>

typedef struct {
    lv_obj_t * lines[60];
    lv_signal_cb_t prev_signal_cb;
} tick_circle_ext_t;

lv_obj_t * create_tick_circle(lv_obj_t * screen);

#endif
