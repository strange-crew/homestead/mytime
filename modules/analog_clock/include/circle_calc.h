#ifndef CIRCLE_CALC_H
#define CIRCLE_CALC_H

#include <lvgl/lvgl.h>

/**
 * returned point assumes center is 0,0.  negative is up and left
 * angle 0 is 12 o'clock
 */
lv_point_t calc_circle_point(uint16_t radius, uint16_t angle_degrees);



#endif
