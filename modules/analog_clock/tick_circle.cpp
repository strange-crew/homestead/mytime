#include <tick_circle.h>
#include <circle_calc.h>
#include <stdio.h>


static lv_point_t hour_tick_points[60][2];

static lv_coord_t coord_(int i) { return (lv_coord_t)i; }

static lv_point_t translate_(lv_point_t mid, lv_point_t pt)
{
    return {
        coord_(mid.x + pt.x),
        coord_(mid.y + pt.y)
    };
}


static lv_res_t tick_circle_signal_cb_(lv_obj_t * obj, lv_signal_t sig, void * param)
{
    auto ext = (tick_circle_ext_t*)lv_obj_get_ext_attr(obj);
    if (ext->prev_signal_cb != nullptr)
    {
        auto res = ext->prev_signal_cb(obj, sig, param);
        if (res != LV_RES_OK)
            return res;
    }

    if (sig == LV_SIGNAL_COORD_CHG)
    {
        auto height = lv_obj_get_height(obj);
        auto width = lv_obj_get_width(obj);

        auto radius = height;
        if (width < height) radius = width;

        radius = radius / 2;

        auto mid_x = width / 2;
        auto mid_y = height / 2;

        auto inner_radius_hr = (decltype(radius))(radius - (radius * .2));
        auto inner_radius_min = radius - (radius * .05);
        lv_point_t mid = { coord_(mid_x), coord_(mid_y) };

        for (auto i = 0; i < 60; ++i)
        {
            auto inner = i % 5 == 0 ? inner_radius_hr : inner_radius_min;
            hour_tick_points[i][0] = translate_(mid, calc_circle_point(radius, i * 6));
            hour_tick_points[i][1] = translate_(mid, calc_circle_point(inner, i * 6));
            lv_line_set_points(ext->lines[i], hour_tick_points[i], 2);
        }


    }

    return LV_RES_OK;
}

lv_obj_t * create_tick_circle(lv_obj_t * parent)
{
    puts("creating");
    auto circle = lv_obj_create(parent, nullptr);
    puts("obj created");
    auto ext = (tick_circle_ext_t*)lv_obj_allocate_ext_attr(circle, sizeof(tick_circle_ext_t));
    puts("ext created");
    for (auto i = 0; i < 60; ++i) ext->lines[i] = nullptr;
    puts("null set");

    static lv_style_t line_style;
    lv_style_init(&line_style);
    lv_style_set_line_width(&line_style, LV_STATE_DEFAULT, 3);
    lv_style_set_line_color(&line_style, LV_STATE_DEFAULT, LV_COLOR_WHITE);

    for (auto i = 0; i < 60; ++i)
    {
        printf("creating line %d\n", i);
        ext->lines[i] = lv_line_create(circle, nullptr);
        if (ext->lines[i] == nullptr)
            puts("not created");
        else
            puts("created");
        lv_obj_add_style(ext->lines[i], LV_LINE_PART_MAIN, &line_style);
        puts("style set");
    }
    ext->prev_signal_cb = lv_obj_get_signal_cb(circle);

    puts("setting callback");
    lv_obj_set_signal_cb(circle, tick_circle_signal_cb_);

    return circle;
}
