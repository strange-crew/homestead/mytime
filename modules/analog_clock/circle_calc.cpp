#include <circle_calc.h>

static lv_coord_t sin_table_[] = {

       0,  174,  348,  523,  697,  871, 1045, 1218, 1391, 1564, 
    1736, 1908, 2079, 2249, 2419, 2588, 2756, 2923, 3090, 3255, 
    3420, 3583, 3746, 3907, 4067, 4226, 4383, 4539, 4694, 4848, 
    4999, 5150, 5299, 5446, 5591, 5735, 5877, 6018, 6156, 6293, 
    6427, 6560, 6691, 6819, 6946, 7071, 7193, 7313, 7431, 7547, 
    7660, 7771, 7880, 7986, 8090, 8191, 8290, 8386, 8480, 8571, 
    8660, 8746, 8829, 8910, 8987, 9063, 9135, 9205, 9271, 9335, 
    9396, 9455, 9510, 9563, 9612, 9659, 9702, 9743, 9781, 9816, 
    9848, 9876, 9902, 9925, 9945, 9961, 9975, 9986, 9993, 9998, 

};

template < typename T >
lv_coord_t coord_(T value) { return value; }

lv_point_t calc_circle_point(uint16_t radius, uint16_t angle_degrees)
{
    while (angle_degrees > 359) angle_degrees -= 360;

    (void)sin_table_;

    if (angle_degrees == 0)
    {
        return { 0, coord_(-(int)radius) };
    }
    if (angle_degrees < 90)
    {
        return { 
            coord_(radius * sin_table_[angle_degrees] / 10000),
            coord_(-(radius * sin_table_[90 - angle_degrees] / 10000))
        };
    }
    if (angle_degrees == 90)
    {
        return {coord_(radius), 0};
    }
    if (angle_degrees < 180)
    {
        angle_degrees -= 90;
        return {
            coord_(radius * sin_table_[90 - angle_degrees] / 10000),
            coord_(radius * sin_table_[angle_degrees] / 10000)
        };
    }
    if (angle_degrees == 180)
    {
        return { 0, coord_(radius) };
    }
    if (angle_degrees < 270)
    {
        angle_degrees -= 180;
        return {
            coord_(-(radius * sin_table_[angle_degrees] / 10000)),
            coord_(radius * sin_table_[90 - angle_degrees] / 10000)
        };
    }
    if (angle_degrees == 270)
    {
        return { coord_(-(int)radius), 0 };
    }

    angle_degrees -= 270;
    return {
        coord_(-(radius * sin_table_[90 - angle_degrees] / 10000)),
        coord_(-(radius * sin_table_[angle_degrees] / 10000))
    };
}
