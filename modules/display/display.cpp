#include <board.h>
#include <display.h>
#include "lv_conf.h"
#include <lvgl/lvgl.h>
#include <st7789.h>
#include <st7789_params.h>


static st7789_t dev;
static lv_disp_drv_t lv_disp;
static lv_disp_buf_t lv_buf;
static lv_color_t buf[2][LV_HOR_RES_MAX * 10];

static void disp_flush_(lv_disp_drv_t * disp, lv_area_t const* area, lv_color_t * color)
{
    st7789_pixmap(&dev, area->x1, area->x2, area->y1, area->y2, (uint16_t*)color);
    lv_disp_flush_ready(disp);
}

void initialize_display()
{
    st7789_init(&dev, &st7789_params[0]);
    lv_init();

    // done by board_init as well, but should do here
    // to really init the display.
    gpio_init(LCD_BACKLIGHT_LOW, GPIO_OUT);
    gpio_init(LCD_BACKLIGHT_MID, GPIO_OUT);
    gpio_init(LCD_BACKLIGHT_HIGH, GPIO_OUT);

    lv_disp_buf_init(&lv_buf, buf[0], buf[1], LV_HOR_RES_MAX * 10);
    lv_disp_drv_init(&lv_disp);
    lv_disp.flush_cb = disp_flush_;
    lv_disp.buffer = &lv_buf;
    lv_disp.hor_res = LV_HOR_RES_MAX;
    lv_disp.ver_res = LV_VER_RES_MAX;
    lv_disp_drv_register(&lv_disp);

    gpio_clear(LCD_BACKLIGHT_LOW);
    gpio_set(LCD_BACKLIGHT_MID);
    gpio_set(LCD_BACKLIGHT_HIGH);

    st7789_fill(&dev, 0, 240, 0, 240, 0);
}
